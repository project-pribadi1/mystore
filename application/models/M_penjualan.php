<?php

class M_penjualan extends CI_Model
{

    public $item = 'item';
    public $penjualan = 'penjualan';
    public $penjualan_detail = 'penjualan_detail';


    function __construct()
    {
        parent::__construct();
    }

    public function get_data()
    {
        $this->db->select('a.*');
        $data = $this->db->get($this->penjualan . ' as a');
        return $data;
    }

    function get_report_penjualan($start_date = null, $end_date = null, $item = null)
    {
        $this->db->select('a.invoice, a.customer, a.created_at, a.remark, a.created_by, m.unit, m.name as item, d.qty, d.price, d.unit, d.id as detail_id');
        $this->db->join($this->penjualan_detail . ' as d', 'a.id = d.penjualan_id', 'left');
        $this->db->join($this->item . ' as m', 'm.id = d.item_id', 'left');
        if ($start_date) {
            $this->db->where("a.tanggal BETWEEN '{$start_date}' AND '{$end_date}'");
        }
        if ($item) {
            $this->db->where("d.item_id", $item);
        }
        $this->db->order_by('a.created_at', 'asc');
        $data = $this->db->get($this->penjualan . ' as a');
        return $data;
    }
}
