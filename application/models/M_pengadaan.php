<?php

class M_pengadaan extends CI_Model
{

    public $pengadaan = 'pengadaan';
    public $pengadaan_detail = 'pengadaan_detail';
    public $vendor = 'vendor';
    public $item = 'item';

    function __construct()
    {
        parent::__construct();
    }

    public function get_data()
    {
        $this->db->select('a.*');
        $data = $this->db->get($this->pengadaan . ' as a');
        return $data;
    }

    function get_report_pengadaan($start_date = null, $end_date = null, $item = null)
    {
        $this->db->select('a.invoice, a.created_at, m.name,  d.id, d.qty, d.price, d.unit, a.created_by');
        $this->db->join($this->pengadaan_detail . ' as d', 'a.id = d.pengadaan_id', 'left');
        $this->db->join($this->item . ' as m', 'm.id = d.item_id', 'left');
        if ($start_date) {
            $this->db->where("a.created_at BETWEEN '{$start_date}' AND '{$end_date}'");
        }
        if ($item) {
            $this->db->where("d.item_id", $item);
        }
        $this->db->order_by('a.created_at', 'asc');
        $data = $this->db->get($this->pengadaan . ' as a');
        return $data;
    }
}
