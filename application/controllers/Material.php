<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Material extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('email')) {
            redirect('auth');
        }
        $this->load->model('m_material');
    }

    public function index()
    {
        $data['material'] = $this->m_material->get_material()->result();
        $data['active'] = 'material';
        $data['title'] = 'Material';
        $data['subview'] = 'material/stock';
        $this->load->view('template/main', $data);
    }

    function add()
    {
        $this->db->trans_begin();
        $price = $this->input->post('price');
        $keterangan = $this->input->post('keterangan');

        $id = $this->input->post('id');
        $data = [
            'name' => $this->input->post('nama'),
            'unit' => $this->input->post('satuan'),
            'price' => replace_angka($price),
            'remark' => replace_angka($keterangan),
            'is_active' => 1,
        ];

        if ($id) {
            $data['update_at'] = date('Y-m-d H:i:s');
            $this->db->update('material', $data, ['id' => $id]);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Material berhasil diperbarui !</div>');
        } else {
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['created_by'] = $this->session->userdata('id');
            $this->db->insert('item', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Material baru berhasil disimpan !</div>');
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
        }
        redirect('material');
    }


    function delete($id)
    {
        if ($id) {
            $this->db->delete('item', ['id' => $id]);
        }
        redirect('material');
    }


    function get_data($id)
    {
        if ($id) {
            $data = $this->m_material->get_material($id)->row();
            echo json_encode($data);
        }
    }

    function kartu_stock()
    {
        $this->form_validation->set_rules('start_date', 'Tanggal Mulai', 'trim|required');
        $this->form_validation->set_rules('end_date', 'Tanggal Akhir', 'trim|required');
        $this->form_validation->set_rules('material', 'Material', 'trim|required');


        if ($this->form_validation->run() == false) {
            $data['kartu_stock'] = null;
        } else {
            $start_date = $this->input->post('start_date');
            $end_date = $this->input->post('end_date');
            $material = $this->input->post('material');
            $data['kartu_stock'] = $this->m_material->get_kartu_stock($start_date, $end_date, $material)->result();
        }
        $data['material'] = $this->m_material->get_material()->result();
        $data['active'] = 'material/kartu_stock';
        $data['title'] = 'Material';
        $data['subview'] = 'material/kartu_stock';
        $this->load->view('template/main', $data);
    }

    function report_stock()
    {
        check_persmission_pages($this->session->userdata('group_id'), 'material/report_stock');
        $this->form_validation->set_rules('start_date', 'Tanggal Mulai', 'trim|required');
        $this->form_validation->set_rules('end_date', 'Tanggal Akhir', 'trim|required');
        $this->form_validation->set_rules('material', 'Material', 'trim|required');


        if ($this->form_validation->run() == false) {
            $start_date = '';
            $end_date = '';
            $material = '';
            $data['kartu_stock'] = $this->m_material->get_report_stock()->result();
        } else {
            $start_date = $this->input->post('start_date');
            $end_date = $this->input->post('end_date');
            $material = $this->input->post('material');
            $data['kartu_stock'] = $this->m_material->get_report_stock($start_date, $end_date, $material)->result();
        }
        $data['start_date'] = $start_date;
        $data['end_date'] = $end_date;
        $data['material_id'] = $material;
        $data['material'] = $this->m_material->get_material()->result();
        $data['active'] = 'material/report_stock';
        $data['title'] = 'Report Material';
        $data['subview'] = 'material/report';
        $this->load->view('template/main', $data);
    }

    function print_report($start_date, $end_date, $material_id)
    {

        $data['start_date'] = $start_date;
        $data['end_date'] = $end_date;
        $data['material_id'] = $material_id;
        $data['kartu_stock'] = $this->m_material->get_report_stock($start_date, $end_date, $material_id)->result();
        $this->load->view('material/print_report', $data);
    }
}
