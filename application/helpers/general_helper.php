<?php

function get_parent_menu($id)
{
    $CI = get_instance();
    $CI->db->select('title');
    $CI->db->where('id', $id);
    $data = $CI->db->get('menus')->row();

    if (!empty($data->title)) {
        return $data->title;
    } else {
        return 'Parent Menu';
    }
}

function log_r($string = null, $var_dump = false)
{
    if ($var_dump) {
        var_dump($string);
    } else {
        echo "<pre>";
        print_r($string);
    }
    exit;
}

function check_persmission_pages($id_group, $link)
{
    $CI = get_instance();
    $CI->db->select('*');
    $CI->db->from('user_access_role A');
    $CI->db->join('menus B', 'A.menu_id = B.id');
    $CI->db->where('A.group_id', $id_group);
    $CI->db->where('B.link', $link);
    $data = $CI->db->get();

    if ($data->num_rows() > 0) {
        return true;
    } else {
        $CI->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"> Anda tidak memilik akses !</div>');
        if ($CI->session->userdata('group_id') == 3) {
            redirect('pos');
        } else {
            redirect('home');
        }
    }
}

function check_permision_menu($id_group, $link)
{
    $CI = get_instance();
    $CI->db->select('*');
    $CI->db->from('user_access_role A');
    $CI->db->join('menus B', 'A.menu_id = B.id');
    $CI->db->where('A.group_id', $id_group);
    $CI->db->where('B.link', $link);
    $data = $CI->db->get();

    if ($data->num_rows() > 0) {
        return true;
    } else {
        return false;
    }
}

function check_persmission_views($id_group, $menu_id)
{
    $CI = get_instance();
    $CI->db->select('*');
    $CI->db->from('user_access_role');
    $CI->db->where('group_id', $id_group);
    $CI->db->where('menu_id', $menu_id);
    $data = $CI->db->get();

    if ($data->num_rows() > 0) {
        return true;
    } else {
        return false;
    }
}

function check_menu($id_group, $menu_id)
{
    $CI = get_instance();
    $CI->db->select('*');
    $CI->db->from('user_access_role');
    $CI->db->where('group_id', $id_group);
    $CI->db->where('menu_id', $menu_id);
    $data = $CI->db->get();

    if ($data->num_rows() > 0) {
        return 'checked';
    }
}

function cek_status($status)
{
    if ($status == 1) {
        return '<span class="badge badge-succees">Done</span>';
    } else if ($status == 2) {
        return '<span class="badge badge-danger">Pending</span>';
    } else {
        return '<span class="badge badge-warning">On progress</span>';
    }
}

function get_user_name($id)
{
    $CI = get_instance();
    $CI->db->select('name');
    $CI->db->where('id', $id);
    $data = $CI->db->get('users')->row();
    if ($data->name) {
        return $data->name;
    } else {
        return 'default';
    }
}

function get_item_name($id)
{
    $CI = get_instance();
    $CI->db->select('nama');
    $CI->db->where('id', $id);
    $data = $CI->db->get('item')->row();
    if ($data->name) {
        return $data->nama;
    } else {
        return 'default';
    }
}

function replace_angka($angka)
{
    return str_replace(",", "", $angka);
}



function get_customer_name($id)
{
    $CI = get_instance();
    $CI->db->select('nama');
    $CI->db->where('id', $id);
    $data = $CI->db->get('customer')->row();
    if ($data->nama) {
        return $data->nama;
    } else {
        return 'default';
    }
}

function get_stock_in($item_id)
{
    $CI = get_instance();
    $CI->db->select('sum(qty) as stock_in');
    $CI->db->where('item_id', $item_id);
    $data = $CI->db->get('pengadaan_detail')->row();
    if ($data->stock_in) {
        return $data->stock_in;
    } else {
        return 0;
    }
}

function get_stock_out($item_id)
{
    $CI = get_instance();
    $CI->db->select('sum(qty) as stock_out');
    $CI->db->where('item_id', $item_id);
    $data = $CI->db->get('penjualan_detail')->row();
    if ($data->stock_out) {
        return $data->stock_out;
    } else {
        return 0;
    }
}

function get_count_vendor()
{
    $CI = get_instance();
    $data = $CI->db->get('vendor')->num_rows();

    return $data;
}

function get_count_customer()
{
    $CI = get_instance();
    $data = $CI->db->get('customer')->num_rows();

    return $data;
}

function get_count_transaction()
{
    $CI = get_instance();
    $data = $CI->db->get('penjualan')->num_rows();

    return $data;
}

function get_count_item()
{
    $CI = get_instance();
    $data = $CI->db->get('item')->num_rows();

    return $data;
}

function get_price_penjualan($penjualan_id)
{
    $CI = get_instance();
    $CI->db->select('sum(price * qty) as total_price');
    $CI->db->where('penjualan_id', $penjualan_id);
    $data = $CI->db->get('penjualan_detail')->row();

    return $data->total_price;
}

function get_qty_penjualan($penjualan_id)
{
    $CI = get_instance();
    $CI->db->select('sum(qty) as total_qty');
    $CI->db->where('penjualan_id', $penjualan_id);
    $data = $CI->db->get('penjualan_detail')->row();

    return $data->total_qty;
}

function get_price_pengadaan($pengadaan_id)
{
    $CI = get_instance();
    $CI->db->select('sum(price * qty) as total_price');
    $CI->db->where('pengadaan_id', $pengadaan_id);
    $data = $CI->db->get('pengadaan_detail')->row();

    return $data->total_price;
}


function get_qty_pengadaan($pengadaan_id)
{
    $CI = get_instance();
    $CI->db->select('sum(qty) as total_qty');
    $CI->db->where('pengadaan_id', $pengadaan_id);
    $data = $CI->db->get('pengadaan_detail')->row();

    return $data->total_qty;
}

function get_vendor_name($id)
{
    $CI = get_instance();
    $CI->db->select('nama');
    $CI->db->where('id', $id);
    $data = $CI->db->get('vendor')->row();
    if ($data->nama) {
        return $data->nama;
    } else {
        return 'default';
    }
}
