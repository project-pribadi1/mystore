<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Dashboard v2</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard v2</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Info boxes -->
            <div class="row">
                <?= $this->session->flashdata('message'); ?>
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box">
                        <span class="info-box-icon bg-info elevation-1"><i class="fas fa-tag"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Vendor</span>
                            <span class="info-box-number">
                                <?= get_count_vendor() ?>
                            </span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box mb-3">
                        <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-clipboard-list"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Item</span>
                            <span class="info-box-number"><?= get_count_item() ?></span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->

                <!-- fix for small devices only -->
                <div class="clearfix hidden-md-up"></div>

                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box mb-3">
                        <span class="info-box-icon bg-success elevation-1"><i class="fas fa-shopping-cart"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Transaction</span>
                            <span class="info-box-number"><?= get_count_transaction() ?></span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box mb-3">
                        <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-users"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Customer</span>
                            <span class="info-box-number"><?= get_count_customer() ?></span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row">
                <!--  -->
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- Main row -->

            <!-- /.row -->
        </div>
        <!--/. container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->


<div class="modal fade" id="modal-panduan">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Panduan MyStore</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row ml-5">
                    <ul class="list-unstyled">
                        <li>Menu Home
                            <ul>
                                <li>Data Item</li>
                                <li>Data Sales</li>
                                <li>Data Vendor</li>
                                <li>Data Customer</li>
                            </ul>
                        </li>
                        <li>Menu Item
                            <ul>
                                <li>Data Item Master & stock</li>
                                <li>Data Report stock history</li>
                            </ul>
                        </li>
                        <li>Menu Vendor
                            <ul>
                                <li>Data Vendor</li>
                            </ul>
                        </li>
                        <li>Menu Penjualan
                            <ul>
                                <li>Add penjualan</li>
                                <li>Data penjualan</li>
                                <li>Data Report History penjualan</li>
                            </ul>
                        </li>
                        <li>Menu Pengadaan
                            <ul>
                                <li>Add Pengadaan</li>
                                <li>Data Pengadaan</li>
                                <li>Data Report History Pengadaan</li>
                            </ul>
                        </li>
                        <li>Menu Customer
                            <ul>
                                <li>Data Customer</li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<script>
    $(document).ready(function() {
        $('#modal-panduan').modal("show");
    });
</script>